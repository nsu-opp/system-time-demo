#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>

int main(void) {
    const int start_seconds = time(NULL);
    sleep(10 /* seconds */);
    const int end_seconds = time(NULL);

    printf("Program run time: %ds\n", end_seconds - start_seconds);

    return EXIT_SUCCESS;
}